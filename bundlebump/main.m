//
//  main.m
//  bundlebump
//
//  Created by James Dumay on 5/05/13.
//  Copyright (c) 2013 James Dumay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WHBundleBump.h"

int main(int argc, const char * argv[])
{

    int returnValue = 0;
    @autoreleasepool {
        returnValue = [[WHBundleBump alloc] execute];
    }
    return 0;
}
