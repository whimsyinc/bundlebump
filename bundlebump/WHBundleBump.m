//
//  WHBundleBump.m
//  bundlebump
//
//  Created by James Dumay on 5/05/13.
//  Copyright (c) 2013 James Dumay. All rights reserved.
//

#import "WHBundleBump.h"

@implementation WHBundleBump

-(int)execute
{
    NSArray *arguments = [[NSProcessInfo processInfo] arguments];
    
    if (arguments.count != 3)
    {
        [self printHelp];
        return 1;
    }
    
    NSString *filePath = arguments[1];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        [self print:[NSString stringWithFormat:@"File does not exist at '%@'", filePath]];
        return 1;
    }
    
    NSString *key = arguments[2];
    
    NSMutableDictionary *contents = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    NSString *keyValue = contents[key];
    if (keyValue)
    {
        NSInteger versionNumber = [keyValue integerValue];
        versionNumber++;
        NSString *newKeyValue = [NSString stringWithFormat:@"%lu", versionNumber];
        [contents setObject:newKeyValue forKey:key];
        if ([contents writeToFile:filePath atomically:YES])
        {
            [self print:[NSString stringWithFormat:@"Updated key '%@' in plist '%@' from '%@' to '%@'", key, filePath, keyValue, newKeyValue]];
        }
        else
        {
            [self print:@"Could not write to file"];
            return 1;
        }
    }
    else
    {
        [self print:[NSString stringWithFormat:@"Could not find key '%@' in plist '%@'", key, filePath]];
        return 1;
    }
    
    return 0;
}

-(void)printHelp
{
    printf("bundlebump [plist path] [key to increment]");
}

-(void)print:(NSString*)str
{
    printf("%s\n", [str UTF8String]);
}

@end
